(function($){
  $(function(){
    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.slider').slider({full_width: true});
    new Vue({
      el: '#rosters',
      data: {
        rosters: []
      },
      created: function () {
        var that = this;
        Tabletop.init(
          {
            // roster https://docs.google.com/spreadsheets/d/1ZAJPG3hlCw_GCTMFsyA41HLL5XSef9iWUDvAiLo1rUQ/pubhtml
            // sermons :https://docs.google.com/spreadsheets/d/1bibq6RjedVjmJj2qL0LmlVbPp0se2nbrzkWQna1MAgM/pubhtml
            key: 'https://docs.google.com/spreadsheets/d/1ZAJPG3hlCw_GCTMFsyA41HLL5XSef9iWUDvAiLo1rUQ/pubhtml',
            callback: function(data, tabletop) {
              that.rosters = data;
            },
            simpleSheet: true
          })
      }
    });

    new Vue({
      el: '#prayerList',
      data: {
        prayerLists: []
      },
      created: function () {
        var that = this;
        Tabletop.init(
          {
            // roster https://docs.google.com/spreadsheets/d/1ZAJPG3hlCw_GCTMFsyA41HLL5XSef9iWUDvAiLo1rUQ/pubhtml
            // sermons :https://docs.google.com/spreadsheets/d/1bibq6RjedVjmJj2qL0LmlVbPp0se2nbrzkWQna1MAgM/pubhtml
            key: 'https://docs.google.com/spreadsheets/d/1qX_YFVLXPiz7gBY7t-i_PlkKlfpkf9P9Ise5lVz20pw/pubhtml',
            callback: function(data, tabletop) {
              that.prayerLists = data;
            },
            simpleSheet: true
          })
      }
    });
  }); // end of document ready
})(jQuery); // end of jQuery name space
