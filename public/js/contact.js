function mapRoute(start) {
  var src = 'https://www.google.com/maps/embed/v1/directions?key=AIzaSyBtg8DmxVc51bO6ns6Kx_nqn-YYHHi78rM&origin=' + start +  '&destination=5 Fenton St Midway Point TAS 7171&avoid=tolls';
  document.getElementById('route').contentWindow.location.replace(src);

}

function initAutocomplete(){

  var autocomplete = new google.maps.places.Autocomplete(
    (document.getElementById('autocomplete')),
    { types: ['geocode'] });
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      //console.log(place);
      if (place.formatted_address) {
        var geo = {};
        var start = place.formatted_address;
        mapRoute(start);
      }else{
        alert("Try again !Please select the dropping address");
      }

    });
}

function LocateMe() {
  window.navigator.geolocation.getCurrentPosition(woSuccess,woError);
}

var woSuccess = function (postion) {
    var geo = {};
    geo.lat = postion.coords.latitude;
    geo.lng = postion.coords.longitude;
    var latlng = new google.maps.LatLng(geo.lat, geo.lng);
    new google.maps.Geocoder().geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var start = results[1].formatted_address;
        document.getElementById('autocomplete').value =start;
        mapRoute(start);
      }
    });
  };

var woError=function(error) {
    alert(error.message);
    console.log(error);
};
(function($){
  $(function(){
    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.slider').slider({full_width: true});
    $.timeliner({});
    $("#locateMe").click(function () {
      LocateMe();
    });
    initAutocomplete();
  }); // end of document ready
})(jQuery); // end of jQuery name space
