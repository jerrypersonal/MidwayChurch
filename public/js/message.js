(function($){
  $(function(){
    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.slider').slider({full_width: true});
    new Vue({
      el: '#mdmess',
      data: {
        messages: []
      },
      created: function () {
        var that = this;
        Tabletop.init(
          {
            // roster https://docs.google.com/spreadsheets/d/1ZAJPG3hlCw_GCTMFsyA41HLL5XSef9iWUDvAiLo1rUQ/pubhtml
            // sermons :https://docs.google.com/spreadsheets/d/1bibq6RjedVjmJj2qL0LmlVbPp0se2nbrzkWQna1MAgM/pubhtml
            key: 'https://docs.google.com/spreadsheets/d/1bibq6RjedVjmJj2qL0LmlVbPp0se2nbrzkWQna1MAgM/pubhtml',
            callback: function(data, tabletop) {
              console.log(data)
              that.messages = data.reverse();
            },
            simpleSheet: true
          });
      }
    });
  }); // end of document ready
})(jQuery); // end of jQuery name space
